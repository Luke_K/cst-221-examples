#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

pthread_mutex_t mutex;
long counter;
time_t end_time;

/*
 * The Counter thread simply increments a count that is protected by a mutex.
 */
void *counter_thread (void *arg)
{
    int status;

    // For the specificed time duration increment the counter, sleep for second with the mutext locked (so monitor can run), and then exit
    while (time (NULL) < end_time)
    {
        // ***** ENTER CRITICAL REGION *****
        status = pthread_mutex_lock (&mutex);
        if (status == 0)
            printf ("Counter Thread: Locked mutex for Counter so it can be incremented\n");
        ++counter;
        status = pthread_mutex_unlock (&mutex);
        if (status == 0)
            printf ("Counter Thread: Unlocked mutex for Counter since we are done with the Counter\n");
        // ***** EXIT CRITICAL REGION *****

        // Sleep to give Monitor Thread a chance to run
        sleep (1);
    }

    // Print the final value of the Counter
    printf ("Final Counter is %lu\n", counter);
    return NULL;
}

/*
 * The Monitor thread simply tries to get the mutex and if locked keeps track of the Misses count else accesses the Counter and prints it out.
 */
void *monitor_thread (void *arg)
{
    int status;
    int misses = 0;

    // For the specificed time duration try to get the mutext and if locked count it else access the counter and print it
    while (time (NULL) < end_time)
    {
        // Sleep to give the Counter thread a chance to run
        sleep (3);

        // Try to get the mutex and if not busy access the Counter to print it else keep track of misses
        status = pthread_mutex_trylock (&mutex);
        if (status != EBUSY)
        {
            printf ("     Monitor Thread: the trylock will lock the mutex so we an safely read the Counter\n");
            printf ("     Monitor Thread: The Counter from Monitor is %ld\n", counter);
            status = pthread_mutex_unlock (&mutex);     // !!! Remember to unlock the mutex !!!
            if (status == 0)
                printf ("     Monitor Thread: will now unlock the mutex since we are done with the Counter\n");
        }
        else
        {
            // Count Misses on the lock
            ++misses;
        }
    }

    // Print number of Misses out
    printf ("Final Monitor Thread missed was %d times.\n", misses);
    return NULL;
}

/*
 * Application entry point to demonstrate the use of mutex trywait API.
 */
int main (int argc, char *argv[])
{
    int status;
    pthread_t counter_thread_id;
    pthread_t monitor_thread_id;

    // Initialize the mutex
    pthread_mutex_init(&mutex, 0);

    // Set the end time for 60 seconds from now
    end_time = time (NULL) + 60;

    // Create the Counter and Monitor Threads
    if(pthread_create (&counter_thread_id, NULL, counter_thread, NULL))
        printf ("Create counter thread failed");
    if(pthread_create (&monitor_thread_id, NULL, monitor_thread, NULL))
        printf ("Created monitor thread failed");

    // Wait for Counter and Monitor Threads to both finish
    if(pthread_join (counter_thread_id, NULL))
        printf ("Joined counter thread failed");
    if (pthread_join (monitor_thread_id, NULL))
        printf ("Joined monitor thread failed");

    // Exit OK
    return 0;
}