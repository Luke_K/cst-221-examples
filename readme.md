This set of code files are used to demonstrate various types of operating systems concepts in C.


Some extra C related resources for this course are as follows:


Tutorials:

* http://c-language.com/

* http://www.tutorialspoint.com/cprogramming/index.htm

* http://publications.gbdirect.co.uk/c_book/

* http://aelinik.free.fr/c/

* http://learn-c.org/

* http://www.wibit.net/course/C


Links for multi-process (Fork-exec)

* http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html

* http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/create.html

* http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/exec.html

* http://advancedlinuxprogramming.com/listings/chapter-3/fork-exec.c

* https://github.com/angrave/SystemProgramming/wiki/Forking,-Part-1:-Introduction

* https://github.com/angrave/SystemProgramming/wiki/Forking,-Part-2:-Fork,-Exec,-Wait


Links about sending signals

* https://github.com/angrave/SystemProgramming/wiki/Process-Control%2C-Part-1%3A-Wait-macros%2C-using-signals

* https://www.gnu.org/software/libc/manual/html_node/Signaling-Another-Process.html

* http://www.thegeekstuff.com/2012/03/catch-signals-sample-c-code


Links for Pthreads:

* https://macboypro.wordpress.com/2009/05/17/prime-numbers-using-posix-threads-on-linux-in-c/

* (Advanced) https://computing.llnl.gov/tutorials/pthreads/

* http://timmurphy.org/2010/05/04/pthreads-in-c-a-minimal-working-example/


Links about Producer / Consumer

* http://greenteapress.com/thinkos/html/thinkos011.html


Links about Semaphores and Monitors:

* https://www.classes.cs.uchicago.edu/archive/2017/winter/51081-1/LabFAQ/lab7/Semaphores.html

* http://uregina.ca/~douglatr/ENSE%20352/Labs/Lab5/Lab5.html

* https://see.stanford.edu/materials/icsppcs107/23-Concurrency-Examples.pdf

* https://en.wikibooks.org/wiki/Operating_System_Design/Processes/Semaphores