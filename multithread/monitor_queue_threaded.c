#include <pthread.h>   //For pthread
#include <stdio.h>     //For printf
#include <stdlib.h>    //For malloc

/* A thread-safe stack:
   - void stack_init(stack* st, int max_capacity)
   - void stack_add(stack* st, int value)
   - int  stack_remove(stack* st)
   - int  stack_size(stack* st)
   - void stack_clear(stack* st)
   - void stack_destroy(stack* st)
 */

/* Thread-safe stack definitions */

typedef struct {
  int max_capacity;
  int num_items;
  int * container;
  pthread_mutex_t lock;
} stack;


void stack_init(stack* st, int max_capacity) {
  st->max_capacity = max_capacity;
  st->num_items = 0;
  st->container = (int*)malloc(max_capacity * sizeof(int));

  //Set up a mutex by first creating a default set of mutex creation attributes
  pthread_mutexattr_t mutex_attr;
  pthread_mutexattr_init(&mutex_attr);

  //Now initialize the mutex, using the set of attributes
  pthread_mutex_init(&st->lock, &mutex_attr);
}


void stack_add(stack* st, int value) {
  pthread_mutex_lock(&st->lock);
  st->container[st->num_items] = value;
  st->num_items += 1;
  pthread_mutex_unlock(&st->lock);
}


int stack_remove(stack* st) {
  //Item stored in position 0 will have num_items set to 1,
  //so we decrement and then return item.
  pthread_mutex_lock(&st->lock);
  st->num_items -= 1;
  int x = st->container[st->num_items];
  pthread_mutex_unlock(&st->lock);
  return x;
}


int stack_size(stack* st) {
  pthread_mutex_lock(&st->lock);
  int i = st->num_items;
  pthread_mutex_unlock(&st->lock);
  return i;
}


void stack_clear(stack* st) {
  pthread_mutex_lock(&st->lock);
  st->num_items = 0;
  pthread_mutex_unlock(&st->lock);
}


void stack_destroy(stack* st) {
  free(st->container);
  pthread_mutex_destroy(&st->lock);
}

/* Global variables */
stack st;
const int VALUE_COUNT = 10;
const int STACK_CAPACITY = 10;

/* Producer thread */
void* producer(void* arg) {
  int i;
  for (i = 0; i < VALUE_COUNT; ++i) {
    //Insert i into stack
    while (stack_size(&st) >= STACK_CAPACITY) {}
    printf("Adding val: %d\n", i);
    stack_add(&st, i);
  }
  pthread_exit(NULL);
}


/* Consumer thread */
void* consumer(void* arg) {
  int i;
  int val;
  for (i = 0; i < VALUE_COUNT; ++i) {
    //Remove value from stack
    while (stack_size(&st) <= 0) {}
    val = stack_remove(&st);
    printf("Got val:%d\n", val);
  }
  pthread_exit(NULL);
}


/* Main */

int main() {

  pthread_t prod_th;
  pthread_t cons_th;

  int item;

  printf("Starting test\n");
  stack_init(&st, STACK_CAPACITY);

  pthread_create(&prod_th, NULL, producer, NULL);
  pthread_create(&cons_th, NULL, consumer, NULL);

  pthread_join(prod_th, NULL);
  pthread_join(cons_th, NULL);

  printf("Ending test\n");
}
