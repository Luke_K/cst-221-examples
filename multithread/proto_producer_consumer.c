#include <stdio.h>
#include <pthread.h>

/*  Global variable "Bounded buffer"  */
int count = 0;

const int MAX_BUF_SIZE = 2;


/*  Thread functions  */

void* prod_func(void* arg) {
  /*function for the producer thread's main */
  printf("Here I am, the producer!\n");

  /*  Wait for buffer not full  */
  while(count >= MAX_BUF_SIZE) {}

  /*  Produce  */
  printf("In producer, count is %d\n", count);
  count += 1;
  printf("After produce, count is %d\n", count);

  /*  Done  */
  pthread_exit(0);
}

void* cons_func(void* arg) {
  /*function for the producer thread's main */
  printf("Here I am, the consumer!\n");

  /*  Wait for buffer not empty  */
  while(count <= 0) {}

  /*  Consume  */
  printf("In consumer, count is %d\n", count);
  count -= 1;
  printf("After consume, count is %d\n", count);

  /*  Done  */
  pthread_exit(0);
}

int main() {

  pthread_t thr_producer_handle;
  pthread_t thr_consumer_handle;

  int th_prod_creat_stat;
  int th_cons_creat_stat;
  int th_join_status;

  /*  Make a producer thread  */
  th_prod_creat_stat = pthread_create(&thr_producer_handle, //pthread_t* thread
                                      NULL,                 //const pthread_attr_r* attr
			   	      prod_func,            //void* (*start_routine)(void*)
			   	      NULL);                //void* arg

  /*  Make a consumer thread  */
  th_cons_creat_stat = pthread_create(&thr_consumer_handle, //pthread_t* thread
                                      NULL,                 //const pthread_attr_r* attr
			   	      cons_func,            //void* (*start_routine)(void*)
			   	      NULL);                //void* arg

  th_join_status = pthread_join(thr_producer_handle, //pthread_t thread
                                NULL);               //void** retval
  th_join_status = pthread_join(thr_consumer_handle, //pthread_t thread
                                NULL);               //void** retval

  printf("Done processing\n");
  return 0;
}
