#include <pthread.h>   //For pthread
#include <stdio.h>     //For printf
#include <stdlib.h>    //For malloc

/* A thread-safe stack:
   - void stack_init(stack* st, int max_capacity)
   - void stack_add(stack* st, int value)
   - int  stack_remove(stack* st)
   - int  stack_size(stack* st)
   - void stack_clear(stack* st)
   - void stack_destroy(stack* st)
 */

/* Thread-safe stack definitions */

typedef struct {
  int max_capacity;
  int num_items;
  int * container;
} stack;


void stack_init(stack* st, int max_capacity) {
  st->max_capacity = max_capacity;
  st->num_items = 0;
  st->container = (int*)malloc(max_capacity * sizeof(int));
}


void stack_add(stack* st, int value) {
  st->container[st->num_items] = value;
  st->num_items += 1;
}


int stack_remove(stack* st) {
  //Item stored in position 0 will have num_items set to 1,
  //so we decrement and then return item.
  st->num_items -= 1;
  return st->container[st->num_items];
}


int stack_size(stack* st) {
  return st->num_items;
}


void stack_clear(stack* st) {
  st->num_items = 0;
}


void stack_destroy(stack* st) {
  free(st->container);
}

/* Global variables */
stack st;
const int VALUE_COUNT = 10;
const int STACK_CAPACITY = 10;

/* Producer thread */
void* producer(void* arg) {
  int i;
  for (i = 0; i < VALUE_COUNT; ++i) {
    //Insert i into stack
    printf("Adding val: %d\n", i);
    stack_add(&st, i);
  }
  pthread_exit(NULL);
}


/* Consumer thread */
void* consumer(void* arg) {
  int i;
  int val;
  for (i = 0; i < VALUE_COUNT; ++i) {
    //Remove value from stack
    val = stack_remove(&st);
    printf("Got val:%d\n", val);
  }
  pthread_exit(NULL);
}


/* Main */

int main() {

  pthread_t prod_th;
  pthread_t cons_th;

  int item;

  printf("Starting test\n");
  stack_init(&st, STACK_CAPACITY);

  pthread_create(&prod_th, NULL, producer, NULL);
  pthread_create(&cons_th, NULL, consumer, NULL);

  pthread_join(prod_th, NULL);
  pthread_join(cons_th, NULL);

  printf("Ending test\n");
}
