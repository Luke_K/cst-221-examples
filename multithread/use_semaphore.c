#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>

/* Semaphore */

sem_t sem1;

int sum = 0;

void* func1(void* arg) {
  long threadID = (long)arg;
  printf("Hello from thread %d\n", threadID);

  int s_val;
  while (sum < 10) {
    sem_wait(&sem1);

    sem_getvalue(&sem1, &s_val);
    printf("T1: Got semaphore, s_val = %d, sum = %d\n", s_val, sum);

    if (sum < 5) {
      sum += 1;
    }

    printf("T1: Done with semaphore, sum is now %d, releasing\n", sum);
    sem_post(&sem1);
  }
  
  pthread_exit(NULL);
}

void* func2(void* arg) {
  long threadID = (long)arg;
  printf("Hello from thread %d\n", threadID);

  int s_val;
  while (sum < 10) {
    sem_wait(&sem1);

    sem_getvalue(&sem1, &s_val);
    printf("T2: Got semaphore, s_val = %d, sum = %d\n", s_val, sum);

    if (sum >= 5) {
      sum += 1;
    }

    printf("T2: Done with semaphore, sum is now %d, releasing\n", sum);
    sem_post(&sem1);
  }
  pthread_exit(NULL);
}

int main() {

  pthread_t thread1;
  pthread_t thread2;

  int status;
  /* Change initial value (third argument) to 1 to enable the semaphore */
  status = sem_init(&sem1, /* sem_t *sem */
                    0,     /* int pshared */
                    0);    /* unsigned int value */

  status = pthread_create(&thread1, NULL, func1, (void*)1);
  status = pthread_create(&thread2, NULL, func2, (void*)2);

  pthread_exit(NULL);

  return 0;
}
