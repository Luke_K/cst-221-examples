/* Example demonstrating how to pass structs to threads
   - Adding in the semaphore to have each thread with it's own
     communication.
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_THREADS  3
#define MAX_PRIORITY 10

/* Semaphore operations */
/* - Initialize */
/* - Destroy */
/* - Increment / Signal / 'V' / 'Increment and wake any waiting process' */
/* - Decrement / Wait / 'P' / 'Decrement and block if the result is negative' */
/* -  */
/* -  */
/* -  */

struct sem_data {
  /* A semaphore needs a value */
  int sem_data;
  char sem_name[252];  //NAME_MAX - 4
};

/*  Thread data  */

struct thread_data {
  int thread_num;
  int priority;
};

void* thr_func(void* arg) {
  struct thread_data* tdata = (struct thread_data*) arg;
  printf("Hello from thread %d!\n", tdata->thread_num);
  printf("  I'm priority #%d\n", tdata->priority);
}

int main() {

  pthread_t thrd[MAX_THREADS];
  //struct thread_data tdata[MAX_THREADS];
  struct thread_data* tdata = malloc(MAX_THREADS * sizeof(struct thread_data));

  int n;
  int t_status;
  for (n = 0; n < MAX_THREADS; ++n) {
    //Set up the tdata
    tdata[n].thread_num = n;
    tdata[n].priority = MAX_PRIORITY-n;

    printf("Starting up thread #%d\n", tdata[n].thread_num);
    //Start up the thread
    t_status = pthread_create(&thrd[n],   //pthread_t *thread
                              NULL,       //const pthread_attr_t *attr
                              thr_func,   //void *(*start_routine) (void *)
                              &tdata[n]); //void *arg

    printf("Done starting up thread #%d\n", tdata[n].thread_num);
  }

  pthread_exit(NULL);

  return 0;
}
