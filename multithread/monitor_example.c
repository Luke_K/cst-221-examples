/* Thanks to:  */
/*   https://stackoverflow.com/questions/5558921/concurrency-a-monitor-that-implements-semaphores */
#include <pthread.h>  // For pthreads
#include <stdio.h>    // For printf()

#define AMOUNT_TO_ADD 1000000


typedef struct
{
  unsigned int count;
  pthread_mutex_t lock;
  pthread_cond_t cond;
} semaph_t;

int
semaph_init (semaph_t *s, unsigned int n)
{
  s->count = n;
  pthread_mutex_init (&s->lock, 0);
  pthread_cond_init (&s->cond, 0);
  return 0;
}

int
semaph_post (semaph_t *s)
{
  //pthread_mutex_lock (&s->lock); // enter monitor
  if (s->count == 0)
    pthread_cond_signal (&s->cond); // signal condition
  ++s->count;
  //pthread_mutex_unlock (&s->lock); // exit monitor
  return 0;
}

int
semaph_wait (semaph_t *s)
{
  //pthread_mutex_lock (&s->lock); // enter monitor
  while (s->count == 0)
    pthread_cond_wait (&s->cond, &s->lock); // wait for condition
  --s->count;
  //pthread_mutex_unlock (&s->lock); // exit monitor
  return 0;
}

/* Global vars */
semaph_t sem1;
int sum = 0;


/* Threading code */

void* func1(void* arg) {
  long thdID = (long)arg;

  int n;
  for (n = 0; n < AMOUNT_TO_ADD; ++n) {
    semaph_wait(&sem1);

    /* Critical Region */
    sum += 1;

    semaph_post(&sem1);
  }

  pthread_exit(NULL);
}

void* func2(void* arg) {
  long thdID = (long)arg;

  int n;
  for (n = 0; n < AMOUNT_TO_ADD; ++n) {
    semaph_wait(&sem1);

    /* Critical Region */
    sum -= 1;

    semaph_post(&sem1);
  }

  pthread_exit(NULL);
}

int main() {
  semaph_init(&sem1,   /* semaph_t *s */
              1);      /* unsigned in n */
  pthread_t thd1;
  pthread_t thd2;

  int status;
  status = pthread_create(&thd1, NULL, func1, (void*)1);
  status = pthread_create(&thd2, NULL, func2, (void*)2);

  status = pthread_join(thd1, NULL);
  status = pthread_join(thd2, NULL);

  printf("Final sum: %d\n", sum);

  return 0;
}
