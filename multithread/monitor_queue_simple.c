#include <pthread.h>   //For pthread
#include <stdio.h>     //For printf
#include <stdlib.h>    //For malloc

/* A thread-safe stack:
   - void stack_init(stack* st, int max_capacity)
   - void stack_add(stack* st, int value)
   - int  stack_remove(stack* st)
   - int  stack_size(stack* st)
   - void stack_clear(stack* st)
   - void stack_destroy(stack* st)
 */

/* Thread-safe stack definitions */

typedef struct {
  int max_capacity;
  int num_items;
  int * container;
} stack;


void stack_init(stack* st, int max_capacity) {
  st->max_capacity = max_capacity;
  st->num_items = 0;
  st->container = (int*)malloc(max_capacity * sizeof(int));
}


void stack_add(stack* st, int value) {
  st->container[st->num_items] = value;
  st->num_items += 1;
}


int stack_remove(stack* st) {
  //Item stored in position 0 will have num_items set to 1,
  //so we decrement and then return item.
  st->num_items -= 1;
  return st->container[st->num_items];
}


int stack_size(stack* st) {
  return st->num_items;
}


void stack_clear(stack* st) {
  st->num_items = 0;
}


void stack_destroy(stack* st) {
  free(st->container);
}


/* Main */

int main() {
  printf("Starting test\n");

  const int st_cap = 10;
  int item;

  stack st;
  stack_init(&st, st_cap);

  //Add item: 5
  item = 5;
  printf("Adding item to stack: %d\n", item);
  stack_add(&st, item);

  //Add item: 6
  item = 6;
  printf("Adding item to stack: %d\n", item);
  stack_add(&st, item);

  //Add item: 7
  item = 7;
  printf("Adding item to stack: %d\n", item);
  stack_add(&st, item);

  //Print the number of items stored
  printf("There are %d items in the stack\n", stack_size(&st));

  //Print item from stack
  printf("Retrieving item: %d\n", stack_remove(&st));
  //Print item from stack
  printf("Retrieving item: %d\n", stack_remove(&st));
  //Print item from stack
  printf("Retrieving item: %d\n", stack_remove(&st));

  printf("Ending test\n");
  pthread_exit(NULL);
}
