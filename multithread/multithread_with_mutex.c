/* Example demonstrating how to pass structs to threads
   - Adding in the mutex ability to have each thread with it's own
     mutual exclusion region
 */

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_THREADS  3
#define MAX_PRIORITY 10
#define ADD_AMOUNT 100000

/* Global variables */
long sum;
pthread_mutex_t mut1 = PTHREAD_MUTEX_INITIALIZER;

struct thread_data {
  int thread_num;
  int priority;
};

void* thr_func(void* arg) {
  struct thread_data* tdata = (struct thread_data*) arg;
  printf("Hello from thread %d!\n", tdata->thread_num);
  printf("  I'm priority #%d\n", tdata->priority);

  long i;
  for (i = 0; i < ADD_AMOUNT; ++i) {
    pthread_mutex_lock(&mut1);
    sum += 1;
    pthread_mutex_unlock(&mut1);
  }
  pthread_exit(NULL);
}

int main() {

  pthread_t thrd[MAX_THREADS];
  //struct thread_data tdata[MAX_THREADS];
  struct thread_data* tdata = malloc(MAX_THREADS * sizeof(struct thread_data));

  sum = 0;

  int n;
  int t_status;
  for (n = 0; n < MAX_THREADS; ++n) {
    //Set up the tdata
    tdata[n].thread_num = n;
    tdata[n].priority = MAX_PRIORITY-n;

    printf("Starting up thread #%d\n", tdata[n].thread_num);
    //Start up the thread
    t_status = pthread_create(&thrd[n],   //pthread_t *thread
                              NULL,       //const pthread_attr_t *attr
                              thr_func,   //void *(*start_routine) (void *)
                              &tdata[n]); //void *arg

    printf("Done starting up thread #%d\n", tdata[n].thread_num);
  }

  for (n = 0; n < MAX_THREADS; ++n) {
    pthread_join(thrd[n], NULL);
  }
  printf("Final sum: %d\n", sum);
  printf(" from %d threads each adding %d\n", MAX_THREADS, ADD_AMOUNT);

  return 0;
}
