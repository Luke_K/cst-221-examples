#include <stdio.h>

#include "tax_functions.h"


int main() {

  printf("Beginning in main()\n");

  float meal_price = 19.95;
  float tax_rate = 0.085;
  float tip_percent = 0.15;

  float final_price = calculate_tax_and_tip(meal_price, tax_rate, tip_percent);

  printf("The meal cost %f, and the taxes are %f%%\n", meal_price, tax_rate*100);
  printf("and the tip is %f%%\n", tip_percent);
  printf("the final (total) cost is %f\n", final_price);

  printf("Done in main()\n");

  return 0;
}
