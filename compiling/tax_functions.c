#include <stdio.h>

float calculate_tax_and_tip(float meal_price,
                            float tax_rate,
			    float tip_percent) {

  printf("In calculate_tax_and_tip: beginning\n");

  float tax_amount = meal_price * tax_rate;
  //printf("  tax amount: %f\n", tax_amount);
  float tip_amount = meal_price * tip_percent;
  //printf("  tip amount: %f\n", tip_amount);
  float total = meal_price + tax_amount + tip_amount;
  //printf("  total amount: %f\n", total);

  printf("In calculate_tax_and_tip: ending\n");

  return total;
}
