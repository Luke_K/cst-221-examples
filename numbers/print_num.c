#include <stdio.h>
#include <string.h>  /* For strlen */
#include <stdlib.h>  /* For malloc, free */

void print_base(int n, int base) {

  char num_str[41] = "                                        ";

  int count = 0;
  int temp = n;
  while (temp > 0) {
    count += 1;
    temp /= 2;
  }
  printf("len = %d\n", count);

  temp = n;
  num_str[count] = 0;
  for (; count > 0; --count) {
    int val = temp % 2;
    temp /= 2;
    printf("digit is %d\n", val);
    num_str[count-1] = (char)('0' + val);
  }

  printf("number '%d' in base %d is '%s'\n", n, base, num_str);
}

int conv_str_to_int(char* s, int len, int base) {
  int total = 0;

  int place = 1;

  int i;
  for (i = len-1; i >= 0; --i) {
    printf("i: %d, char in pos i: %c (byte: %d)\n", i, s[i], s[i]);
    int n = (int)(s[i] - '0');
    total += (n * place);

    place *= base;
    printf("Total is now %d\n", total);
  }

  return total;
}

int main() {

  //char num_str[10];
  char* num_str = (char*)malloc(33);
  int num;

  printf("Enter a number(base 10):");
  scanf("%s", num_str);
  printf("  You entered %s\n", num_str);

  printf("The first character is '%c'\n", num_str[0]);
  int len = strlen(num_str);
  printf("The length of the string is %d\n", len);

  num = conv_str_to_int(num_str, len, 10);

  //num = conv_str_to_int(num_str, len, 2);

  //print_base(num, 2);
  print_base(num, 10);
  //print_base(num, 16);

  free(num_str);

  return 0;
}
