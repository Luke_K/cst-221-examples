#include <pthread.h>      //For pthreads
#include <stdio.h>        //For printf
#include <semaphore.h>    //For semaphores
#include <unistd.h>       //For usleep

/* Global constants */
const int NUM_REPS = 5;

/* Global variables */
sem_t resource_1;
sem_t resource_2;

int count_A = 0;
int count_B = 0;

/* Thread functions */

void operation_a() {
  /* Get resource 1 */
  sem_wait(&resource_1);

  usleep(100000);  //Arg is number of microseconds

  /* Get resource 2 */
  sem_wait(&resource_2);

  /* Perform operation */
  printf("Performing thread A operation: %d\n", count_A++);

  /* Release resources (order doesn't matter here) */
  sem_post(&resource_1);
  sem_post(&resource_2);
}

void operation_b() {
  /* Get resource 1 */
  sem_wait(&resource_1);

  usleep(100000);  //Arg is number of microseconds

  /* Get resource 2 */
  sem_wait(&resource_2);

  /* Perform operation */
  printf("Performing thread B operation: %d\n", count_B++);

  /* Release resources (order doesn't matter here) */
  sem_post(&resource_1);
  sem_post(&resource_2);
}

void* thread_a_func() {
  printf("Thread A starting...\n");
  int i;
  for (i = 0; i < NUM_REPS; ++i) {
    operation_a();
  }

  /* Done processing */
  printf("Thread A done processing. Exiting...\n");
  pthread_exit(0);
}

void* thread_b_func() {
  printf("Thread B starting...\n");
  int i;
  for (i = 0; i < NUM_REPS; ++i) {
    operation_b();
  }

  /* Done processing */
  printf("Thread B done processing. Exiting...\n");
  pthread_exit(0);
}

/* Main */

int main() {

  pthread_t thread_a;
  pthread_t thread_b;
  pthread_t thread_timer;

  sem_init(&resource_1, 0, 1);
  sem_init(&resource_2, 0, 1);

  printf("Creating threads\n");

  pthread_create(&thread_a, NULL, thread_a_func, NULL);
  pthread_create(&thread_b, NULL, thread_b_func, NULL);

  pthread_exit(0);

  return 0;
}
