#!/usr/bin/env bash
echo Begin

input_filename=$1
group_name=$2
op_flag=$3

echo op_flag: $op_flag

if [[ $op_flag == "-a" ]] 
then
  echo Got an \'add user\' command
  echo "  using the $input_filename"
  adduser john --quiet
  
fi

if [[ $op_flag == "-r" ]]
then
  echo Got a \'Remove user\' command
fi

echo end
