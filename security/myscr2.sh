echo Running my script 2

value="This is a test"

if [[ $value =~ T.* ]]
then
  echo "$value" begins with a T
fi

if [[ $value =~ This......test ]]
then
  echo Starts with \'Test\' and ends with \'test\'
else
  echo Something else
fi

if [[ $value =~ This.{2,6}test ]]
then
  echo Starts with \'Test\' and ends with \'test\'
else
  echo Something else
fi

if [[ $value =~ ^.{14}$ ]]
then
  echo value is 14 characters long
fi

if [[ $value =~ ^.{6}\  ]]
then
  echo 7th char is a space
fi

if [[ $value =~ ^.{7}\  ]]
then
  echo 8th char is a space
fi

if [[ $value =~ [STUV] ]]
then
  echo value contains a capital S, T, U, or V
fi
