echo This is a test

name=Luke

echo Hello, $name

if [[ $name==Luke ]]
then
  echo Welcome
else
  echo Who are you?
fi

if [[ $name =~ L.* ]]
then
  echo Starts with 'L'
else
  echo Doesn\'t start with 'L'
fi

if [[ $name =~ A.* ]]
then
  echo Starts with 'A'
else
  echo Doesn\'t start with 'A'
fi


name2=Abigail
echo Working with name2, $name2

if [[ $name2 =~ L.* ]]
then
  echo Starts with 'L'
else
  echo Doesn\'t start with 'L'
fi

if [[ $name2 =~ A.* ]]
then
  echo Starts with 'A'
else
  echo Doesn\'t start with 'A'
fi


