#include <stdio.h>
#include <sys/types.h> 
#include <sys/wait.h> 
#include <unistd.h> 
#include <stdlib.h>
 
int main(int argc, char * argv[]) 
{ 
   int pid = -1; 
 
   // Create a child process 
   pid = fork(); 
 
   if(pid == 0) 
   { 
     printf("\nI am the child. My pid is %d.\n", getpid());
      return 0; 
   } 
   else if(pid > 0) 
   { 
     printf("\nI am the parent. My pid is %d.\n", getpid());
   } 
   else 
   { 
     fprintf(stderr, "Fork error. Quitting Program.\n");
 
      // Bail out of the program. 
      exit(EXIT_FAILURE); 
   } 
 
   // The parent will wait for a child to die before proceeding.  The child 
   // will never get here, because the child quits in the if statement above. 
   wait(0); 
   printf("\nChild just died.\n");
 
   // Now, the child is dead for sure, and we are back to one process. 
 
   printf("\n\n");
   printf("\nStarting phase 2.\n");
   printf("\n\n");
 
   // Fork a process, and have the child execute an ls. 
   pid = fork(); 
 
   if(pid == 0) 
   { 
      printf("\nI am the child. My pid is %d.\n", getpid());
      //printf("\n\n", getpid());
      printf("\nThe child will now exec an ls command.\n", getpid());
 
      // Print a line of stars above the ls. 
      printf("\n****************************************");
      printf("****************************************\n");
      char *argm[] = {"ls", "-la", 0}; 
      
      execvp(argm[0], argm);
 
      printf("This statement should not be executed if execvp is successful.\n");
      fprintf(stderr, "The exec command issued by the child failed!!!  Exiting.\n");
      exit(EXIT_FAILURE); 
   } 
   else if(pid > 0) 
   { 
      wait(0); 
 
      // We know for sure that the child is dead, so print a line of stars 
      // after the ls. 
      printf("\nParent: ****************************************");
      printf("****************************************\n");
   } 
   else 
   { 

      fprintf(stderr, "Fork error. Quitting Program.\n");
 
      // Bail out of the program. 
      exit(EXIT_FAILURE); 
   } 
 
   return 0; 
} 
