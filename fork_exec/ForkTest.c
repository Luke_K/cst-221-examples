#include <stdio.h>
#include <string.h>

// Required by for routine
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>   // Declaration for exit()

int globalVariable = 2;

main()
{
   char sIdentifier[100];
   int  iStackVariable = 20;

   pid_t pID = fork();
   if (pID == 0)                // child
   {
      // Code only executed by child process

      strcpy(sIdentifier, "Child Process:");
      globalVariable++;
      iStackVariable++;
    }
    else if (pID < 0)            // failed to fork
    {
        fprintf(stderr, "Failed to fork\n");
        exit(1);
        // Throw exception
    }
    else                                   // parent
    {
      // Code only executed by parent process

      strcpy(sIdentifier, "Parent Process:");
    }

    // Code executed by both parent and child.
  
    printf("%s Global variable: %d Stack variable: %d\n",
           sIdentifier, globalVariable, iStackVariable);
}
