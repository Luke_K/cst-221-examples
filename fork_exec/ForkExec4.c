#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/*
  Not working yet. Child ls call not printing anything.
*/

int main() {
  pid_t parent = getpid();
  pid_t pid = fork();
  
  if (pid == -1)
  {
      /* error, failed to fork() */
      fprintf(stderr, "Error: Failed to fork!\n");
      exit(1);
  } 
  else if (pid > 0)
  {
      /*We are the parent */
      printf("I'm the parent (pid:%d)\n", parent);
      printf(" - Calling waitpid on pid: %d\n", pid);
      int status;
      waitpid(pid, &status, 0);
      printf(" - Done waiting.\n");
  }
  else 
  {
      /* we are the child */
      printf("I'm the child (pid:%d)\n", pid);
      char * const args = "";
      execve("/bin/ls", &args, 0);
      _exit(EXIT_FAILURE);   /* exec never returns */
  }
}
