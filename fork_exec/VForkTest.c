//#include <iostream>
//#include <string>
#include <stdio.h>
#include <string.h>

// Required by for routine
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

/*

  Example code demonstrating vfork(), a fork call that blocks the parent while
  the child runs _using the parents memory!_ The parent then resumes where the
  child left off, and finishes. See linux man page "man vfork" for more detail.

*/

int globalVariable = 2;

int main()
{
   char sIdentifier[100];
   int  iStackVariable = 20;

   pid_t pID = vfork();
   if (pID == 0)                // child
   {
      // Code only executed by child process

      strcpy(sIdentifier, "Child Process: ");
      globalVariable += 1;
      iStackVariable += 1;
      printf("%s Global variable: %d Stack variable: %d\n",
             sIdentifier, globalVariable, iStackVariable);
      _exit(0);
    }
    else if (pID < 0)            // failed to fork
    {
        fprintf(stderr, "Failed to fork\n");
        exit(1);
        // Throw exception
    }
    else                                   // parent
    {
      // Code only executed by parent process

      strcpy(sIdentifier, "Parent Process: ");
    }

    // executed only by parent

      printf("%s Global variable: %d Stack variable: %d\n",
             sIdentifier, globalVariable, iStackVariable);
    exit(0);

    return 0;
}
