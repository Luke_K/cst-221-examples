/* Taken from (and modified):
 https://stackoverflow.com/questions/3585846/color-text-in-terminal-applications-in-unix  
 See https://en.wikipedia.org/wiki/ANSI_escape_code
 and search for "Colors" for a few more codes.
 */

#include <stdio.h>

/* Constants for use in the "Proper" section */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

/* Constants for use in the "Efficient" section.
   Note the overlap of the values. */
#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define BLU   "\x1B[34m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define WHT   "\x1B[37m"
#define RESET "\x1B[0m"

int main()
{
  /* "Proper" way to change colors */
  printf("Proper way:\n");

  printf("%sred\n", KRED);
  printf("%sgreen\n", KGRN);
  printf("%syellow\n", KYEL);
  printf("%sblue\n", KBLU);
  printf("%smagenta\n", KMAG);
  printf("%scyan\n", KCYN);
  printf("%swhite\n", KWHT);
  printf("%snormal\n", KNRM);

  /* "Efficient" way to change colors.
     This uses the normal compiler effect of concatenating adjacent string literals. */

  printf("\nEfficient way:\n");

  printf(RED "red\n" RESET);
  printf(GRN "green\n" RESET);
  printf(YEL "yellow\n" RESET);
  printf(BLU "blue\n" RESET);
  printf(MAG "magenta\n" RESET);
  printf(CYN "cyan\n" RESET);
  printf(WHT "white\n" RESET);

  return 0;
}
